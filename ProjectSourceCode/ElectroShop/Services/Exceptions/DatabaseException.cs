﻿using Services.AbstractResponseFactory;
using Services.ConcreteResponseFactory;
using Services.Interfaces;

namespace Services.Exceptions
{
    public class DatabaseException : GenericException
    {
        public DatabaseException()
        {
            base.ErrorCode = "500";
            base.ErrorDescription = "InternalServerError";
        }
        public DatabaseException(string errorCode, string errorDescription)
        {
            base.ErrorCode = errorCode;
            base.ErrorDescription = errorDescription;
        }

        public override ICustomResponse GetResponse()
        {
            ResponseFactory factory = new DatabaseResponseFactory();
            ResponseAssembler assembler = new ResponseAssembler();
            return assembler.AssembleResponse(factory);
        }
    }
}
