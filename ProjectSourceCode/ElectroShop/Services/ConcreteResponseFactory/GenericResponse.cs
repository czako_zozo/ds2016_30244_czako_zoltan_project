﻿using System.Net;
using Services.Interfaces;

namespace Services.ConcreteResponseFactory
{
    class GenericResponse : ICustomResponse
    {
        private const string GenericMessage = "Generic error!";
        private const HttpStatusCode Status = HttpStatusCode.InternalServerError;

        public string Message => GenericMessage;
        public HttpStatusCode StatusCode => Status;
    }
}
