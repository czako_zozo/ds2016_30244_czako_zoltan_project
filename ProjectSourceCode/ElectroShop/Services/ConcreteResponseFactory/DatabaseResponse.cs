﻿using System.Net;
using Services.Interfaces;

namespace Services.ConcreteResponseFactory
{
    public class DatabaseResponse : ICustomResponse
    {
        private const string DatabaseMessage = "Database error!";
        private const HttpStatusCode Status = HttpStatusCode.InternalServerError;

        public string Message => DatabaseMessage;
        public HttpStatusCode StatusCode => Status;
    }
}
