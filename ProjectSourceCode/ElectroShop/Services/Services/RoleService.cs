﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;
        public RoleService(IRoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return roleRepository.GetAllRoles();
        }

        public bool HasRight(User user, string roleName)
        {
            return roleRepository.hasRight(user, roleName);
        }
    }
}