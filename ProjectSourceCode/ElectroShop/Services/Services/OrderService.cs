﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Net;
using Repository.DatabaseEntities;
using Repository.Interfaces;
using Services.Exceptions;

namespace Services.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public void InsertOrder(Order product)
        {
            try
            {
                this.orderRepository.Insert(product);
                this.orderRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Insret order failed!");
            }
        }

        public void DeleteOrder(Order product)
        {
            try
            {
                this.orderRepository.Delete(product);
                this.orderRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Delete order failed!");
            }
        }

        public void UpdateOrder(Order product)
        {
            try
            {
                this.orderRepository.Update(product);
                this.orderRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Edit order failed!");
            }
        }

        public IEnumerable<Order> GetOrdersByUserId(int userId)
        {
            return this.orderRepository.GetOrdersByUserId(userId);
        }


        public Order GetOrderById(int id)
        {
            return this.orderRepository.GetOrderById(id);
        }
    }
}