﻿using System;
using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Repository.DatabaseEntities;
using Services.Exceptions;

namespace Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;
        private readonly IStockRepository stockRepository;
        public ProductService(IProductRepository productRepository, IStockRepository stockRepository)
        {
            this.productRepository = productRepository;
            this.stockRepository = stockRepository;
        }
        public IEnumerable<Product> GetAllProducts()
        {
            return this.productRepository.GetAllProducts();
        }

        public Product GetProductById(int id)
        {
            return this.productRepository.GetAllProducts().FirstOrDefault(p => p.id == id);
        }

        public void InsertProduct(Product product)
        {
            try
            {
                this.productRepository.Insert(product);
                this.productRepository.Save();
            }
            catch(Exception ex)
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Insert product failed!");
            }
        }

        public void DeleteProduct(Product product)
        {
            try
            {
                Stock stock = stockRepository.GetAllStock().FirstOrDefault(s => s.product_id == product.id);
                this.stockRepository.Delete(stock);
                this.stockRepository.Save();
                this.productRepository.Delete(product);
                this.productRepository.Save();
            }
            catch(Exception ex)
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Delete product failed!");
            }
        }

        public void UpdateProduct(Product product)
        {
            try
            {
                this.productRepository.Update(product);
                this.productRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Edit product failed!");
            }
        }
    }
}