﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Repository.DatabaseEntities;
using Repository.Interfaces;
using Services.Exceptions;
using Services.Interfaces;

namespace Services.Services
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository ratingRepository;

        public RatingService(IRatingRepository ratingRepository)
        {
            this.ratingRepository = ratingRepository;
        }

        public int GetRatingByProductId(int productId)
        {
            var ratings = this.ratingRepository.GetRatingByProductId(productId);
            int sum = 0;
            int numberOfRatings = 0;

            foreach (Rating rating in ratings)
            {
                if (rating.Rating1.HasValue)
                {
                    numberOfRatings++;
                    sum += rating.Rating1.Value;
                }
            }

            if (numberOfRatings == 0)
                return 0;

            return (int)Math.Round((double)sum/numberOfRatings, MidpointRounding.AwayFromZero);
        }

        public void Insert(Rating rating)
        {
            try
            {
                this.ratingRepository.Insert(rating);
                this.ratingRepository.Save();
            }
            catch (Exception ex)
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Insert rating failed!");
            }
        }
    }
}
