﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Net;
using Repository.DatabaseEntities;
using Services.Exceptions;

namespace Services.Services
{
    public class StockService : IStockService
    {
        private readonly IStockRepository stockRepository;
        public StockService(IStockRepository stockRepository)
        {
            this.stockRepository = stockRepository;
        }

        public void UpdateProductStock(Stock stock)
        {
            try
            {
                stockRepository.Update(stock);
                stockRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.InternalServerError.ToString(), "Edit stock failed!");
            }
        }

        public IEnumerable<Stock> GetAllStock()
        {
            return stockRepository.GetAllStock();
        }


        public void CreateStock(Stock stock)
        {
            this.stockRepository.Insert(stock);
            this.stockRepository.Save();
        }


        public Stock GetById(int id)
        {
            return this.stockRepository.GetById(id);
        }


        public void DeleteStock(Stock stock)
        {
            this.stockRepository.Delete(stock);
            stockRepository.Save();
        }

    }
}