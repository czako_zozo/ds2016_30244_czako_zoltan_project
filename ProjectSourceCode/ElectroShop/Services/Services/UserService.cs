﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Repository.DatabaseEntities;
using Repository.Interfaces;
using Services.Exceptions;

namespace Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IRatingRepository ratingRepository;

        public UserService(IUserRepository userRepository, IRatingRepository ratingRepository)
        {
            this.userRepository = userRepository;
            this.ratingRepository = ratingRepository;
        }

        public void InsertUser(User userToInsert)
        {
            try
            {
                userRepository.Insert(userToInsert);
                userRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.Conflict.ToString(), "Insert user failed!");
            }
        }

        public void UpdateUser(User userToUpdate)
        {
            try
            {
                userRepository.Update(userToUpdate);
                userRepository.Save();
            }
            catch
            {
                throw new DatabaseException(HttpStatusCode.Conflict.ToString(), "Edit user failed!");
            }
        }

        public void DeleteUser(User userToDelete)
        {
            try
            {
                IEnumerable<Rating> ratings = ratingRepository.GetAll();
                var ratingsOfUser = ratings.Where(r => r.UserId == userToDelete.id);

                foreach (var rating in ratingsOfUser)
                {
                    rating.User = null;
                    ratingRepository.Update(rating);
                    ratingRepository.Save();
                }

                userRepository.Delete(userToDelete);
                userRepository.Save();
            }
            catch(Exception ex)
            {
                throw new DatabaseException(HttpStatusCode.Conflict.ToString(), "Delete user failed!");
            }
        }

        public User GetUserById(int id)
        {
            return userRepository.GetById(id);
        }

        public IEnumerable<User> GetAllUser()
        {
            return userRepository.GetAll();
        }

        public User GetByUsername(String username)
        {
            return userRepository.GetByUsername(username);
        }


        public void Save()
        {
            userRepository.Save();
        }
    }
}