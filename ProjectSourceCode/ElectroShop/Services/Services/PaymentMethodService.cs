﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Services
{
    public class PaymentMethodService : IPaymentMethodService
    {
        private readonly IPaymentMethodRepository paymentMethodRepository;

        public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository)
        {
            this.paymentMethodRepository = paymentMethodRepository;
        }
        public IEnumerable<PaymentMethod> getAllMethods()
        {
            return this.paymentMethodRepository.getAllMethods();
        }
    }
}