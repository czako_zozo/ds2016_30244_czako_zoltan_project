﻿using ElectronRepository.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;
        public CategoryService(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }
        public int GetCategoryByName(string categoryName)
        {
            return this.categoryRepository.GetCategoryByName(categoryName);
        }

        public IEnumerable<Category> GetAll()
        {
            return categoryRepository.GetAll();
        }
    }
}