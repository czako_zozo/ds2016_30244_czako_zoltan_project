﻿namespace Services.Interfaces
{
    public interface IException
    {
        ICustomResponse GetResponse();
    }
}
