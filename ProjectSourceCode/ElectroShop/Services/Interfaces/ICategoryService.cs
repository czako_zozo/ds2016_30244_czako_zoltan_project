﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Interfaces
{
    public interface ICategoryService
    {
        int GetCategoryByName(string categoryName);
        IEnumerable<Category> GetAll();
    }
}
