﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Interfaces
{
    public interface IOrderService
    {
        void InsertOrder(Order product);
        void DeleteOrder(Order product);
        void UpdateOrder(Order product);
        IEnumerable<Order> GetOrdersByUserId(int userId);
        Order GetOrderById(int id);
    }
}
