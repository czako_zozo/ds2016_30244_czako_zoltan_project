﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Interfaces
{
    public interface IRatingService
    {
        int GetRatingByProductId(int productId);
        void Insert(Rating rating);
    }
}
