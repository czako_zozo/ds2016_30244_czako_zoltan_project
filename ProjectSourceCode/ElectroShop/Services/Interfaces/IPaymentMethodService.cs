﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Interfaces
{
    public interface IPaymentMethodService
    {
        IEnumerable<PaymentMethod> getAllMethods();
    }
}
