﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Services.Interfaces
{
    public interface IStockService
    {
        void UpdateProductStock(Stock stock);
        IEnumerable<Stock> GetAllStock();
        void CreateStock(Stock stock);
        Stock GetById(int id);
        void DeleteStock(Stock stock);
    }
}
