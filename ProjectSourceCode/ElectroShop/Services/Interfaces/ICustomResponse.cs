﻿using System.Net;

namespace Services.Interfaces
{
    public interface ICustomResponse
    {
        string Message { get; }
        HttpStatusCode StatusCode { get; }
    }
}
