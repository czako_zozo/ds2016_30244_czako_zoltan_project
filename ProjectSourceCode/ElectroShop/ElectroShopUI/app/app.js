﻿(function () {
    "use strict";

    var app = angular.module("electroShopManagement",
    [
        "common.services",
        "ui.router"
    ]);

    app.config(
    [
        "$stateProvider", "$urlRouterProvider",
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "app/modules/home/views/home.html",
                    controller: "HomeCtrl as vm"
                })
                .state("about", {
                    url: "/about",
                    templateUrl: "app/modules/about/about.html"
                })
                .state("contact", {
                    url: "/contact",
                    templateUrl: "app/modules/contact/contact.html"
                })
                .state("productList", {
                    url: "/products",
                    templateUrl: "app/modules/products/views/productList.html",
                    controller: "ProductListCtrl as vm"
                })
                .state("productDetails", {
                    url: "/products/details/:id",
                    templateUrl: "app/modules/products/views/productDetails.html",
                    controller: "ProductDetailsCtrl as vm"
                })
                .state("productEdit", {
                    url: "/products/:id",
                    templateUrl: "app/modules/products/views/productEdit.html",
                    controller: "ProductEditCtrl as vm"
                })
                .state("productDelete", {
                    url: "/products/delete/:id",
                    templateUrl: "app/modules/products/views/productDelete.html",
                    controller: "ProductDeleteCtrl as vm"
                })
                .state("userList", {
                    url: "/users",
                    templateUrl: "app/modules/users/views/userList.html",
                    controller: "UserListCtrl as vm"
                })
                .state("userEdit", {
                    url: "/users/:id",
                    templateUrl: "app/modules/users/views/userEdit.html",
                    controller: "UserListCtrl as vm"
                })
                .state("userRegister", {
                    url: "/register",
                    templateUrl: "app/modules/users/views/userRegister.html",
                    controller: "UserRegisterCtrl as vm"
                })
                .state("userDelete", {
                    url: "/users/delete/:id",
                    templateUrl: "app/modules/users/views/userDelete.html",
                    controller: "UserDeleteCtrl as vm"
                })
                .state("ordersOfUser", {
                    url: "/orders/orders-of-user/:id",
                    templateUrl: "app/modules/orders/views/ordersOfUser.html",
                    controller: "OrdersOfUserCtrl as vm"
                })
                .state("shoppingCart", {
                    url: "/shopping-cart/:id",
                    templateUrl: "app/modules/shoppingCart/views/shoppingCart.html",
                    controller: "ShoppingCartCtrl as vm"
                })
                .state("payment", {
                    url: "/payments",
                    templateUrl: "app/modules/payment/views/paymentList.html",
                    controller: "PaymentCtrl as vm"
                })
                .state("stocks", {
                    url: "/stocks",
                    templateUrl: "app/modules/stocks/views/stockList.html",
                    controller: "StockListCtrl as vm"
                })
                .state("stockEdit", {
                    url: "/stocks/edit/:id",
                    templateUrl: "app/modules/stocks/views/stockEdit.html",
                    controller: "StockEditCtrl as vm"
                });
        }]
    );
}());