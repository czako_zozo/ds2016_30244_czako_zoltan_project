﻿(function() {
    "use strict";

    function currentUser() {
        var profile = {
            isLoggedIn: false,
            username: "",
            password: "",
            role: "",
            id : 0
        };

        var setProfile = function(username, password, role, id, isLoggedIn) {
            profile.username = username;
            profile.password = password;
            profile.role = role;
            profile.isLoggedIn = isLoggedIn;
            profile.id = id;
        }

        var getProfile = function() {
            return profile;
        }

        return {
            setProfile: setProfile,
            getProfile: getProfile
        }
    }

    angular
        .module("common.services")
        .factory("currentUser", currentUser);
})();