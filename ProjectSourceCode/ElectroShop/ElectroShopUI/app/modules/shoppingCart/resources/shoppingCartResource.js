﻿(function () {
    "use strict";

    function shoppingCartResource($resource, appSettings) {
        return {
            addToCart: $resource(appSettings.serverPath + "/api/shopping-cart/:id"),
            removeFromCart: $resource(appSettings.serverPath + "/api/remove-from-cart/:id", null, {
                'delete': { method: 'DELETE' }
            })
        }
    }

    angular
        .module("common.services")
        .factory("shoppingCartResource", ["$resource", "appSettings", shoppingCartResource]);
}());