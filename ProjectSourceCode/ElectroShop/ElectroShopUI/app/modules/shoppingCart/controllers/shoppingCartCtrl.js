﻿(function () {
    "use strict";

    function shoppingCartCtrl(shoppingCartResource, currentUser, $state) {
        var vm = this;
        vm.message = "";
        vm.totalPrice = 0;

        shoppingCartResource.addToCart.query({ id: currentUser.getProfile().id },
            function (data) {
                vm.orders = data;
                var total = 0;
                for (var i = 0; i < orders.count; i++) {
                    total += orders[i].Price;
                }
                vm.totalPrice = total;
            });

        vm.remove = function (orderId) {

            shoppingCartResource.removeFromCart.delete({ id: orderId },
                function (data) {
                    shoppingCartResource.addToCart.query({ id: currentUser.getProfile().id },
                    function (data) {
                        vm.orders = data;
                    });
                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }

        vm.pay = function () {
            $state.go("payment");
        }
    }

    angular
        .module("electroShopManagement")
        .controller("ShoppingCartCtrl", ["shoppingCartResource", "currentUser", "$state", shoppingCartCtrl]);
}())