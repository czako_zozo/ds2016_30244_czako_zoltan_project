﻿(function() {
    "use strict";

    function mainCtrl(userAccountResource, currentUser, $state, $window) {
        var vm = this;

        vm.isLoggedIn = function() {
            return currentUser.getProfile().isLoggedIn;
        }

        vm.message = "";
        vm.user = {};
        vm.userData = {
            username : "",
            password : ""
        };

        vm.login = function() {
            userAccountResource.get({ username: vm.userData.username, password: vm.userData.password },
                function (data) {
                    vm.user = data;
                    currentUser.setProfile(vm.user.Username, vm.user.Pass, vm.user.MainRole, vm.user.Id, true);
                    vm.message = "";
                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }

        vm.logout = function() {
            currentUser.setProfile("", "", "", 0, false);
            $window.location.reload();
            $state.go("home");
        }
    }

    angular
        .module("electroShopManagement")
        .controller("MainCtrl", ["userAccountResource", "currentUser", "$state", "$window", mainCtrl]);
}());