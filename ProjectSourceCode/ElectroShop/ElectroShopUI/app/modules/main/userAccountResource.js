﻿(function () {
    "use strict";

    function userAccountResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/get-by-username/:username/:password");
    }

    angular
        .module("common.services")
        .factory("userAccountResource", ["$resource", "appSettings", userAccountResource]);
}());