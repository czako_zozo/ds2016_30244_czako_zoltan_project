﻿(function () {
    "use strict";

    function productDeleteResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/products/:id", null,
        {
            'update': { method: 'PUT' },
            'delete': { method: 'DELETE' }
        });
    }

    angular
        .module("common.services")
        .factory("productDeleteResource", ["$resource", "appSettings", productDeleteResource]);
}());