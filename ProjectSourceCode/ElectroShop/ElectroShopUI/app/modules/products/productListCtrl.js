﻿(function () {
    "use strict";

    function productListCtrl(productResource, currentUser, $state) {
        var vm = this;
        vm.message = "";
        vm.profile = currentUser.getProfile();

        productResource.productList.query(function (data) {
            vm.products = data;
        });

        vm.addProductToCart = function(productId) {
            productResource.addToCart.get({ productId: productId, id: currentUser.getProfile().id },
                function(data) {

                },
                function(response) {
                    vm.message = response.statusText + "\r\n";
                });
        }

        vm.rate = function(productId, productRate) {
            productResource.rating.rate({ productId: productId, rate: productRate },
                function (data) {
                    $state.go("productList");
                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("ProductListCtrl", ["productResource", "currentUser", "$state", productListCtrl]);
}())