﻿(function () {
    "use strict";

    function productDetailsCtrl($stateParams, productDetailsResource, currentUser) {
        var vm = this;
        vm.profile = currentUser.getProfile();

        var productId = $stateParams.id;

        productDetailsResource.details.get({ id: productId }, function (data) {
            vm.product = data;
        });

        vm.addProductToCart = function (productId) {
            productDetailsResource.addToCart.get({ productId: productId, id: currentUser.getProfile().id },
                function (data) {

                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("ProductDetailsCtrl", ["$stateParams", "productDetailsResource", "currentUser", productDetailsCtrl]);
}())