﻿(function () {
    "use strict";

    function productEditCtrl($stateParams, $state, productEditResource ) {
        var vm = this;
        vm.product = {};
        vm.message = '';

        var productId = $stateParams.id;

        productEditResource.get({ id: productId }, function (data) {
            vm.product = data;
            vm.originalProduct = angular.copy(data);
        },
        function (response) {
            vm.message = response.statusText + "\r\n";
        });

        if (vm.product && vm.product.Id) {
            vm.title = "Edit: " + vm.product.Name;
        }
        else {
            vm.title = "New Product";
        }

        vm.submit = function () {
            vm.message = "";

            if (vm.product.Id) {
                vm.product.$update({ model: vm.product },
                    function (data) {
                        $state.go("productList");
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                    });
            }
            else {
                vm.product.$save(
                    function (data) {
                        vm.originalProduct = angular.copy(data);

                        $state.go("productList");
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                    });
            }
        };

        vm.cancel = function (productForm) {
            productForm.$setPristine();
            vm.product = angular.copy(originalProduct);
        };
    }

    angular
        .module("electroShopManagement")
        .controller("ProductEditCtrl", ["$stateParams", "$state", "productEditResource", productEditCtrl]);
}())