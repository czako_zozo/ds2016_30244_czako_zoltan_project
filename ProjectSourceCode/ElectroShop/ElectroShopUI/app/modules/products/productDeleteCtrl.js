﻿(function () {
    "use strict";

    function productDeleteCtrl($stateParams, $state, productDeleteResource) {
        var vm = this;
        vm.product = {};
        vm.message = '';

        var productId = $stateParams.id;

        productDeleteResource.get({ id: productId }, function (data) {
            vm.product = data;
            vm.originalProduct = angular.copy(data);
        },
        function (response) {
            vm.message = response.statusText + "\r\n";
        });

        vm.submit = function () {
            vm.message = "";

                vm.product.$delete({ id: productId },
                    function (data) {
                        $state.go("productList");
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                    });
        };
    }

    angular
        .module("electroShopManagement")
        .controller("ProductDeleteCtrl", ["$stateParams", "$state", "productDeleteResource", productDeleteCtrl]);
}())