﻿(function () {
    "use strict";

    function productEditResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/products/:id", null,
        {
            'update': { method: 'PUT' },
            'save': { method: 'POST' }
        });
    }

    angular
        .module("common.services")
        .factory("productEditResource", ["$resource", "appSettings", productEditResource]);
}());