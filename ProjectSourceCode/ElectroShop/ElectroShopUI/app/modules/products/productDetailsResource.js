﻿(function () {
    "use strict";

    function productDetailsResource($resource, appSettings) {
        return {
            details: $resource(appSettings.serverPath + "/api/products/:id", null,
            {
                'update': { method: 'PUT' }
            }),
            addToCart: $resource(appSettings.serverPath + "/api/add-to-cart/:productId/:id")
        }
    }

    angular
        .module("common.services")
        .factory("productDetailsResource", ["$resource", "appSettings", productDetailsResource]);
}());