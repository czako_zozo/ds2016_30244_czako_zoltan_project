﻿(function () {
    "use strict";

    function productResource($resource, appSettings) {
        return {
            productList: $resource(appSettings.serverPath + "/api/products", null,
            {
                'update': { method: 'PUT' }
            }),
            addToCart: $resource(appSettings.serverPath + "/api/add-to-cart/:productId/:id"),
            rating: $resource(appSettings.serverPath + "/api/ratings/:productId/:rate", null,
            {
                'rate': { method: 'GET' } 
            })
        }
    }

    angular
        .module("common.services")
        .factory("productResource", ["$resource", "appSettings", productResource]);
}());