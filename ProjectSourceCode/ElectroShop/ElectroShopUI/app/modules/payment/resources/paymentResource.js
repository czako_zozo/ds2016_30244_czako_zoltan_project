﻿(function () {
    "use strict";

    function paymentResource($resource, appSettings) {
        return {
            allPayments: $resource(appSettings.serverPath + "/api/payments"),
            finalizePayment: $resource(appSettings.serverPath + "/api/finalize/:paymentId/:userId", null, {
                'finalize': { method: 'GET' }
            })
        }
    }

    angular
        .module("common.services")
        .factory("paymentResource", ["$resource", "appSettings", paymentResource]);
}());