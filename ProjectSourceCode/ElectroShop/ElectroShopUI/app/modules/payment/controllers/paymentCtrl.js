﻿(function () {
    "use strict";

    function paymentCtrl(paymentResource, currentUser, $state) {
        var vm = this;
        vm.message = "";

        paymentResource.allPayments.query(function (data) {
            vm.payments = data;
        });

        vm.finalize = function (paymentId) {
            paymentResource.finalizePayment.finalize({ paymentId: paymentId, userId: currentUser.getProfile().id },
                function (data) {
                    $state.go("productList");
                },
                function (reponse) {
                    vm.message = response.statusText + "\r\n";
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("PaymentCtrl", ["paymentResource", "currentUser", "$state", paymentCtrl]);
}())