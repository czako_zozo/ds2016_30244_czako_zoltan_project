﻿(function () {
    "use strict";

    function userEditCtrl($stateParams, $state, userEditResource) {
        var vm = this;
        vm.user = {
            Username : '',
            Pass: '',
            Firstname : '',
            Lastname: '',
            Mobile: '',
            Mail: '',
            Country: '',
            City: '',
            Street: '',
            Number : ''
        };
        vm.message = '';

        var userId = $stateParams.id;

        if (userId != 0) {
            userEditResource.get({ id: userId }, function (data) {
                vm.user = data;
            },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }

        if (vm.user && vm.user.Id) {
            vm.title = "Edit: " + vm.user.Username;
        }
        else {
            vm.title = "New Product";
        }

        vm.submit = function () {
            vm.message = "";
            if (vm.user.Id) {
                vm.user.$update({ model: vm.user },
                    function (data) {
                        $state.go("userList");
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                    });
            }
            else {
                vm.user.$save(
                    function (data) {
                        $state.go("userList");
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                    });
            }
        };
    }

    angular
        .module("electroShopManagement")
        .controller("UserEditCtrl", ["$stateParams", "$state", "userEditResource", userEditCtrl]);
}())