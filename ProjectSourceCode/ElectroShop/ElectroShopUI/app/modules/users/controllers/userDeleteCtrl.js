﻿(function () {
    "use strict";

    function userDeleteCtrl($stateParams, $state, userDeleteResource) {
        var vm = this;
        vm.user = {};
        vm.message = '';

        var userId = $stateParams.id;

        userDeleteResource.get({ id: userId }, function (data) {
            vm.user = data;
        },
        function (response) {
            vm.message = response.statusText + "\r\n";
        });

        vm.submit = function () {
            vm.message = "";

            vm.user.$delete({ id: userId },
                function (data) {
                    $state.go("userList");
                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        };
    }

    angular
        .module("electroShopManagement")
        .controller("UserDeleteCtrl", ["$stateParams", "$state", "userDeleteResource", userDeleteCtrl]);
}())