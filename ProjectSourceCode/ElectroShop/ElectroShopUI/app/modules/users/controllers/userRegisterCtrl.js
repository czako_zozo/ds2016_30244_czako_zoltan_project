﻿(function() {
    "use strict";
    
    function userRegisterCtrl($stateParams, $state, userRegisterResource) {
        var vm = this;
        vm.user = {
            Username: '',
            Pass: '',
            Firstname: '',
            Lastname: '',
            Mobile: '',
            Mail: '',
            Country: '',
            City: '',
            Street: '',
            Number: ''
        };
        vm.message = '';

        vm.registerUser = function() {
            userRegisterResource.register(vm.user,
                function(data) {
                    $state.go("productList");
                },
                function(response) {
                    vm.message = response.statusText + "\r\n";
                    if (response.data.exceptionMessage)
                        vm.message += response.data.exceptionMessage;

                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            vm.message += response.data.modelState[key] + "\r\n";
                        }
                    }
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("UserRegisterCtrl", ["$stateParams", "$state", "userRegisterResource", userRegisterCtrl]);
})();