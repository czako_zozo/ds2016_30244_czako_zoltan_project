﻿(function () {
    "use strict";

    function userListCtrl(userResource) {
        var vm = this;

        userResource.query(function (data) {
            vm.users = data;
        });
    }

    angular
        .module("electroShopManagement")
        .controller("UserListCtrl", ["userResource", userListCtrl]);
}())