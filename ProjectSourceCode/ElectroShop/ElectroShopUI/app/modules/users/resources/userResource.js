﻿(function () {
    "use strict";

    function userResource($resource, $http, appSettings, currentUser) {

        $http.defaults.headers.common['Authorization'] = 'Basic ' + currentUser.getProfile().username + ":" + currentUser.getProfile().password;

        return $resource(appSettings.serverPath + "/api/users", null,
        {
            'update': {
                method: 'PUT',
                headers: { "Authorization": "Basic" + currentUser.getProfile().username + ":" + currentUser.getProfile().password }
            },
            'save': {
                method: 'POST',
                headers: { "Authorization": "Basic" + currentUser.getProfile().username + ":" + currentUser.getProfile().password }
            }
        });
    }

    angular
        .module("common.services")
        .factory("userResource", ["$resource", "$http", "appSettings", "currentUser", userResource]);
}());