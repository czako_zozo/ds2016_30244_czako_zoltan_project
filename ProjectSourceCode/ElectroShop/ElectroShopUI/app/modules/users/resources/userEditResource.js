﻿(function () {
    "use strict";

    function userEditResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/users/:id", null,
        {
            'save': { method: 'POST' },
            'update': { method: 'PUT' }
        });
    }

    angular
        .module("common.services")
        .factory("userEditResource", ["$resource", "appSettings", userEditResource]);
}());