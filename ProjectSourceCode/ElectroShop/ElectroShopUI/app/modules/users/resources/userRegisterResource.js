﻿(function () {
    "use strict";

    function userRegisterResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/users", null,
        {
            'register': { method: 'POST' }
        });
    }

    angular
        .module("common.services")
        .factory("userRegisterResource", ["$resource", "appSettings", userRegisterResource]);
}());