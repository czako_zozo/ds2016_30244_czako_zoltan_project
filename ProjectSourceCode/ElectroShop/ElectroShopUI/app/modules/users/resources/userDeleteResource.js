﻿(function () {
    "use strict";

    function userDeleteResource($resource, appSettings, currentUser) {

        var headers = { "Authorization": "Basic " + currentUser.getProfile().username + ":" + currentUser.getProfile().password };

        return $resource(appSettings.serverPath + "/api/users/:id", null,
        {
            'delete': {
                method: 'DELETE',
                headers: headers
            }
        });
    }

    angular
        .module("common.services")
        .factory("userDeleteResource", ["$resource", "appSettings", "currentUser", userDeleteResource]);
}());