﻿(function () {
    "use strict";

    function ordersOfUserResource($resource, appSettings) {
        return {
            ordersOfUser: $resource(appSettings.serverPath + "/api/orders/orders-of-user/:id"),
            confirmOrders: $resource(appSettings.serverPath + "/api/confirm/:orderId")
        }
    }

    angular
        .module("common.services")
        .factory("ordersOfUserResource", ["$resource", "appSettings", ordersOfUserResource]);
}());