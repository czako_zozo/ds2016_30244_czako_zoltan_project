﻿(function () {
    "use strict";

    function ordersOfUserCtrl($stateParams, ordersOfUserResource, $state) {
        var vm = this;
        vm.message = "";

        var userId = $stateParams.id;

        ordersOfUserResource.ordersOfUser.query({ id: userId }, function (data) {
            vm.orders = data;
        });

        vm.confirm = function(orderId) {
            ordersOfUserResource.confirmOrders.get({ orderId: orderId},
                function(data) {
                    $state.go("userList");
                },
                function(response) {
                    vm.message = response.statusText + "\r\n";
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("OrdersOfUserCtrl", ["$stateParams", "ordersOfUserResource", "$state", ordersOfUserCtrl]);
}())