﻿(function() {
    "use strict";

    function HomeCtrl(homeResource) {
        var vm = this;

        homeResource.query(function (data) {
            vm.products = data;
        });
    }

    angular
        .module("electroShopManagement")
        .controller("HomeCtrl", ["homeResource", HomeCtrl]);
}())