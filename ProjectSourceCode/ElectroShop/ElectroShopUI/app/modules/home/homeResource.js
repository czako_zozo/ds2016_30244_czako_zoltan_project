﻿(function() {
    "use strict";

    function homeResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/products/get-recent");
    }

    angular
        .module("common.services")
        .factory("homeResource", ["$resource", "appSettings", homeResource]);

}())