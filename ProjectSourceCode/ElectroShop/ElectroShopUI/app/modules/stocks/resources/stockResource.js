﻿(function () {
    "use strict";

    function stockResource($resource, appSettings, currentUser, $http) {

        var headers = { "Authorization": "Basic " + currentUser.getProfile().username + ":" + currentUser.getProfile().password };
        $http.defaults.headers.common['Authorization'] = 'Basic ' + currentUser.getProfile().username + ":" + currentUser.getProfile().password;

        return $resource(appSettings.serverPath + "/api/stocks/:id", null, {
            'get': {
                method: 'GET',
                headers: headers
            },
            'update' : {
                method: 'PUT',
                headers: headers
            }
        })
    }

    angular
        .module("common.services")
        .factory("stockResource", ["$resource", "appSettings", "currentUser", "$http", stockResource]);
}());