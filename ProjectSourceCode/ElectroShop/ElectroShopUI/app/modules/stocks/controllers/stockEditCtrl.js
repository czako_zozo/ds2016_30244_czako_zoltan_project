﻿(function () {
    "use strict";

    function stockEditCtrl(stockResource, $stateParams, $state) {
        var vm = this;

        var stockId = $stateParams.id;

        stockResource.get({ id: stockId },
            function (data) {
                vm.stock = data;
            });

        vm.submit = function () {
            vm.stock.$update({ model: vm.stock },
                function (data) {
                    $state.go("stocks");
                },
                function (response) {
                    vm.message = response.statusText + "\r\n";
                });
        }
    }

    angular
        .module("electroShopManagement")
        .controller("StockEditCtrl", ["stockResource", "$stateParams", "$state", stockEditCtrl]);
}())