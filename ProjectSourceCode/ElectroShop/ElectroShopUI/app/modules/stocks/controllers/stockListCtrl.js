﻿(function () {
    "use strict";

    function stockListCtrl(stockResource) {
        var vm = this;

        stockResource.query(function (data) {
            vm.stocks = data;
        });
    }

    angular
        .module("electroShopManagement")
        .controller("StockListCtrl", ["stockResource", stockListCtrl]);
}())