﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface IUserRepository
    {
        User GetById(int id);
        User GetByUsername(string username);
        IEnumerable<User> GetAll();
        void Delete(User entityToDelete);
        void Update(User entityToUpdate);
        void Insert(User entityToInsert);
        void Save();
    }
}
