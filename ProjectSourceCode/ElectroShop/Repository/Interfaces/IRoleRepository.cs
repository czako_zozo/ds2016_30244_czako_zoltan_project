﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface IRoleRepository
    {
        IEnumerable<Role> GetAllRoles();
        bool hasRight(User user, string roleName);
    }
}
