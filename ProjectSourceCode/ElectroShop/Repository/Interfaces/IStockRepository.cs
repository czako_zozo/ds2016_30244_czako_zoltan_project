﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface IStockRepository
    {
        IEnumerable<Stock> GetAllStock();
        void Update(Stock stock);
        void Insert(Stock stock);
        Stock GetById(int id);
        void Delete(Stock stock);
        void Save();
    }
}
