﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAllProducts();
        Product GetProductById(int id);
        void Insert(Product product);
        void Delete(Product product);
        void Update(Product product);
        void Save();
    }
}
