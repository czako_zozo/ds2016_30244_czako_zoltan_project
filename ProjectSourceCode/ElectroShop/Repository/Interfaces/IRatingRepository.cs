﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Repository.Interfaces
{
    public interface IRatingRepository
    {
        IEnumerable<Rating> GetRatingByProductId(int productId);
        IEnumerable<Rating> GetAll(); 
        void Insert(Rating rating);
        void Update(Rating rating);
        void Save();
    }
}
