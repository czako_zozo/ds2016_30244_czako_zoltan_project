﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface ICategoryRepository
    {
        int GetCategoryByName(string categoryName);
        IEnumerable<Category> GetAll();
    }
}
