﻿using Repository.DatabaseEntities;
using System.Collections.Generic;

namespace ElectronRepository.Interfaces
{
    public interface IPaymentMethodRepository
    {
        IEnumerable<PaymentMethod> getAllMethods();
    }
}
