﻿using System.Collections.Generic;
using Repository.DatabaseEntities;

namespace Repository.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetOrdersByUserId(int userId);
        void Insert(Order product);
        void Delete(Order product);
        void Update(Order product);
        Order GetOrderById(int id);
        void Save();
    }
}
