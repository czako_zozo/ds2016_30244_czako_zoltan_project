﻿using System.Collections.Generic;
using Repository.DatabaseEntities;
using Repository.GenericRepository;
using Repository.Interfaces;

namespace Repository.Repositories
{
    public class RatingRepository : GenericRepository<ElectroShopEntities, Rating>, IRatingRepository
    {
        public IEnumerable<Rating> GetRatingByProductId(int productId)
        {
            return base.Get(r => r.ProductId == productId);
        }

        public IEnumerable<Rating> GetAll()
        {
            return base.Get();
        }

        public void InsertRating(Rating rating)
        {
            
        }
    }
}
