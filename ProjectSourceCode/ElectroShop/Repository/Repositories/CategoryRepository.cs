﻿using System.Collections.Generic;
using System.Linq;
using ElectronRepository.Interfaces;
using Repository.DatabaseEntities;
using Repository.GenericRepository;

namespace Repository.Repositories
{
    public class CategoryRepository : GenericRepository<ElectroShopEntities, Category>, ICategoryRepository
    {
        public int GetCategoryByName(string categoryName)
        {
            return base.Get(c => c.name.Equals(categoryName)).First().id;
        }

        public IEnumerable<Category> GetAll()
        {
            return base.Get();
        }
    }
}