﻿using System.Collections.Generic;
using System.Linq;
using Repository.DatabaseEntities;
using Repository.GenericRepository;
using Repository.Interfaces;

namespace Repository.Repositories
{
    public class OrderRepository : GenericRepository<ElectroShopEntities, Order>, IOrderRepository
    {
        public IEnumerable<Order> GetOrdersByUserId(int userId)
        {
            return base.Get(o => o.user_id == userId);
        }

        public Order GetOrderById(int id)
        {
            return base.Get(o => o.id == id).First();
        }
    }
}