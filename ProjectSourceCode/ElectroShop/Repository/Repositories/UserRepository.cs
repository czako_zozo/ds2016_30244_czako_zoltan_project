﻿using System.Collections.Generic;
using System.Linq;
using ElectronRepository.Interfaces;
using Repository.DatabaseEntities;
using Repository.GenericRepository;

namespace Repository.Repositories
{
    public class UserRepository : GenericRepository<ElectroShopEntities, User>, IUserRepository
    {
        public User GetById(int id)
        {
            return base.Get(u => u.id == id).First();
        }

        public IEnumerable<User> GetAll()
        {
            return base.Get();
        }

        public User GetByUsername(string username)
        {
            var users = base.Get(u => u.username.Equals(username));
            User user = null;
            if(users != null && users.Any())
                user = users.First();
            return user;
        }
    }
}