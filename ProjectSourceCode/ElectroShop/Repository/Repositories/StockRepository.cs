﻿using System.Collections.Generic;
using System.Linq;
using ElectronRepository.Interfaces;
using Repository.DatabaseEntities;
using Repository.GenericRepository;

namespace Repository.Repositories
{
    public class StockRepository : GenericRepository<ElectroShopEntities, Stock>, IStockRepository 
    {
        public IEnumerable<Stock> GetAllStock()
        {
            return base.Get();
        }
        public Stock GetById(int id)
        {
            return base.Get(s => s.id == id).First();
        }
    }
}