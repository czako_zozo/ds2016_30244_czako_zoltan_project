﻿using System.Collections.Generic;
using ElectronRepository.Interfaces;
using Repository.DatabaseEntities;
using Repository.GenericRepository;

namespace Repository.Repositories
{
    public class PaymentMethodRepository : GenericRepository<ElectroShopEntities, PaymentMethod>, IPaymentMethodRepository
    {
        public IEnumerable<PaymentMethod> getAllMethods()
        {
            return base.Get();
        }
    }
}