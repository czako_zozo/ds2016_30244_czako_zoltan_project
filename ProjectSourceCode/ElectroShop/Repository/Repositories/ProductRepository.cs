﻿using System.Collections.Generic;
using System.Linq;
using ElectronRepository.Interfaces;
using Repository.DatabaseEntities;
using Repository.GenericRepository;

namespace Repository.Repositories
{
    public class ProductRepository : GenericRepository<ElectroShopEntities, Product>, IProductRepository
    {
        public IEnumerable<Product> GetAllProducts()
        {
            return base.Get();
        }

        public Product GetProductById(int id)
        {
            return base.Get(p => p.id == id).First();
        }

    }
}