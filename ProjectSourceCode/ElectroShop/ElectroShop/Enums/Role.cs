﻿namespace ElectroShop.Enums
{
    public enum Role
    {
        Client,
        Admin,
        Provider
    }

    public static class RoleExtension
    {
        public static string GetRoleName(this Role role)
        {
            switch (role)
            {
                case Role.Client:
                    return "client";
                case Role.Admin:
                    return "admin";
                case Role.Provider:
                    return "provider";
                default:
                    return "client";
            }
        }
    }
}