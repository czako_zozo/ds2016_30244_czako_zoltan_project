﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using ElectroShop.ExceptionHandlers;
using Services.Interfaces;

namespace ElectroShop.CustomAttributes
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private const HttpStatusCode DefaultStatusCode = HttpStatusCode.InternalServerError;

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is IException)
            {
                var exceptionType = actionExecutedContext.Exception.GetType();
                IException exception = (IException) Activator.CreateInstance(exceptionType);
                CustomExceptionHandler handler = new CustomExceptionHandler(exception);
                var response = handler.Handle();

                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(response.Message, System.Text.Encoding.UTF8, "text/plain"),
                    StatusCode = response.StatusCode
                };
            }
            else
            {
                var message = actionExecutedContext.Exception.Message;
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(message, System.Text.Encoding.UTF8, "text/plain"),
                    StatusCode = DefaultStatusCode
                };
            }

            base.OnException(actionExecutedContext);
        }
    }
}