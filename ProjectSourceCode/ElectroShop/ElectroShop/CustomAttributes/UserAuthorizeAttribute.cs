﻿using System;
using ElectroShop.Enums;

namespace ElectroShop.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class UserAuthorizeAttribute : Attribute
    {
        private readonly Role role;

        public Role Role => role;

        public UserAuthorizeAttribute(Role role)
        {
            this.role = role;
        }
    }
}