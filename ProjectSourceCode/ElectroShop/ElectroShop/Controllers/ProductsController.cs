﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using ElectroShop.CustomAttributes;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;
using RoleEnum = ElectroShop.Enums;

namespace ElectroShop.Controllers
{
    public class ProductsController : ApiController
    {
        private const int StartOfInterval = -60;

        private readonly IProductService productService;
        private readonly IStockService stockService;

        private IMapper modelMapper;
        private IMapper entityMapper;

        public ProductsController(IProductService productService, IStockService stockService)
        {
            this.productService = productService;
            this.stockService = stockService;
            SetupMappers();
        }

        [Route("api/products")]
        public IEnumerable<ProductModel> GetAll()
        {
            var products = this.productService.GetAllProducts();
            var productModels = modelMapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(products);

            return productModels;
        }

        [Route("api/products/{id}")]
        public ProductModel GetById(int id)
        {
            var products = this.productService.GetProductById(id);
            return modelMapper.Map<Product, ProductModel>(products);
        }

        [Route("api/products/get-recent")]
        public IEnumerable<ProductModel> GetRecent()
        {
            var startDate = DateTime.Now.AddDays(StartOfInterval);
            var recentProducts = this.productService.GetAllProducts().Where(m => m.data > startDate);
            var products = modelMapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(recentProducts);

            return products;
        }

        [HttpPost]
  //      [UserAuthorize(RoleEnum.Role.Admin)]
        [Route("api/products")]
        public HttpResponseMessage InsertProduct(ProductModel model)
        {
            var product = entityMapper.Map<ProductModel, Product>(model);
            product.data = DateTime.Now;
            var stock = new Stock { cantity = 0 };
            product.Stocks.Add(stock);
            productService.InsertProduct(product);

            Product lastProduct = productService.GetAllProducts().Last();
            stock.cantity = model.Quantity;
            stockService.UpdateProductStock(stock);

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPut]
  //      [UserAuthorize(RoleEnum.Role.Admin)]
        [Route("api/products")]
        public HttpResponseMessage UpdateProduct(ProductModel model)
        {
            var product = entityMapper.Map<ProductModel, Product>(model);
            productService.UpdateProduct(product);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
 //       [UserAuthorize(RoleEnum.Role.Admin)]
        [Route("api/products/{id}")]
        public HttpResponseMessage DeleteProduct(int id)
        {
            var product = productService.GetProductById(id);

            if (product == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Wrong product id!");

            productService.DeleteProduct(product);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private void SetupMappers()
        {
            var configModelMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Product, ProductModel>().
                ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Stocks.Sum(s => s.cantity)));
                cfg.CreateMap<Category, CategoryModel>();
            });

            var configEntityMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProductModel, Product>();
                cfg.CreateMap<CategoryModel, Category>();
            });

            modelMapper = configModelMapper.CreateMapper();
            entityMapper = configEntityMapper.CreateMapper();
        }
    }
}
