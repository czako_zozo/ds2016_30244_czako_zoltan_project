﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;

namespace ElectroShop.Controllers
{
    public class RatingsController : ApiController
    {
        private readonly IRatingService ratingService;
        private readonly IProductService productService;
        private readonly IUserService userService;

        public RatingsController(IRatingService ratingService, IProductService productService, IUserService userService)
        {
            this.ratingService = ratingService;
            this.productService = productService;
            this.userService = userService;
        }

        [HttpGet]
        [Route("api/ratings/{productId}")]
        public int GetRating(int productId)
        {
            int rating = ratingService.GetRatingByProductId(productId);
            Product product = productService.GetProductById(productId);
            product.rating = rating;
            productService.UpdateProduct(product);

            return rating;
        }

        [HttpGet]
        [Route("api/ratings/{productId}/{rate}")]
        public HttpResponseMessage PostRating(int productId, int rate)
        {
            var newRating = new Rating
            {
                UserId = null,
                ProductId = productId,
                Rating1 = rate
            };
            ratingService.Insert(newRating);

            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
