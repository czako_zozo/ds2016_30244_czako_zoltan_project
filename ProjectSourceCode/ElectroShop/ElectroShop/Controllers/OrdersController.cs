﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using ElectroShop.CustomAttributes;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;
using RoleEnum = ElectroShop.Enums;

namespace ElectroShop.Controllers
{
    public class OrdersController : ApiController
    {
        private const int PendingOrderStatusId = 1;
        private const int ConfirmedOrderStatusId = 2;
        //   private const int RejectedOrderStatusId = 3;
        private const int NewOrderStatusId = 4;

        private readonly IOrderService orderService;
        private readonly IUserService userService;
        private readonly IStockService stockService;
        private readonly IPaymentMethodService paymentMethodService;

        private IMapper modelMapper;

        public OrdersController(IOrderService orderService, IUserService userService,
            IStockService stockService, IPaymentMethodService paymentMethodService)
        {
            this.orderService = orderService;
            this.userService = userService;
            this.stockService = stockService;
            this.paymentMethodService = paymentMethodService;
            SetupMappers();
        }

        [Route("api/orders/orders-of-user/{id}")]
        public IEnumerable<OrderModel> GetOrdersOfUser(int id)
        {
            var user = userService.GetUserById(id);
            var orders = orderService.GetOrdersByUserId(user.id);
            var orderModels = modelMapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(orders);

            return orderModels;
        }

        [Route("api/shopping-cart/{id}")]
        public IEnumerable<OrderModel> GetNewOrdersOfUser(int id)
        {
            var user = userService.GetUserById(id);
            var orders = orderService.GetOrdersByUserId(user.id);
            var newOrders = orders.Where(o => o.OrderState.id == NewOrderStatusId);
            var orderModels = modelMapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(newOrders);

            return orderModels;
        }

        [Route("api/orders/{id}")]
        public OrderModel GetOrderById(int id)
        {
            var order = orderService.GetOrderById(id);
            if (order == null)
                throw new ArgumentException("Invalid order id!");

            var orderModel = modelMapper.Map<Order, OrderModel>(order);

            return orderModel;
        }

        [HttpGet]
        [Route("api/add-to-cart/{productId}/{id}")]
        public HttpResponseMessage AddToCart(int productId, int id)
        {
            var user = userService.GetUserById(id);
            bool inStock = stockService.GetAllStock().Where(s => s.product_id == productId).Any(stock => stock.cantity > 0);

            if (inStock)
            {
                var order = new Order
                {
                    order_state_id = NewOrderStatusId,
                    product_id = productId,
                    user_id = user.id,
                    data = DateTime.Now
                };
                orderService.InsertOrder(order);

                return Request.CreateResponse(HttpStatusCode.Created);
            }

            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Out of stock!");
        }

        [HttpDelete]
        [Route("api/remove-from-cart/{id}")]
        public HttpResponseMessage RemoveFromCart(int id)
        {
            Order order = orderService.GetOrderById(id);
            try
            {
                orderService.DeleteOrder(order);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Conflict, "Cannot remove from cart!");
            };
        }

        [HttpGet]
        [Route("api/finalize/{paymentId}/{userId}")]
        public HttpResponseMessage FinalizeShopping(int paymentId, int userId)
        {
            var user = userService.GetUserById(userId);
            var paymentMethod = paymentMethodService.getAllMethods().FirstOrDefault(p => p.id == paymentId);

            if (paymentMethod == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Wrong payment method");

            var newOrders = orderService.GetOrdersByUserId(user.id).Where(o => o.order_state_id == NewOrderStatusId);
            foreach (var order in newOrders)
            {
                order.order_state_id = PendingOrderStatusId;
                order.payment_id = paymentMethod.id;
                
                var stock = stockService.GetAllStock().FirstOrDefault(s => s.product_id == order.product_id && s.cantity > 0);
                if (stock == null)
                    return  Request.CreateErrorResponse(HttpStatusCode.Conflict, "Product out of stock!");

                stock.cantity--;
                stockService.UpdateProductStock(stock);
                orderService.UpdateOrder(order);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [UserAuthorize(RoleEnum.Role.Admin)]
        [Route("api/confirm/{orderId}")]
        public HttpResponseMessage ConfirmOrder(int orderId)
        {
            var order = orderService.GetOrderById(orderId);
            var stock = stockService.GetAllStock()
                .FirstOrDefault(s => s.product_id == order.Product.id && s.cantity > 0);

            if (stock != null)
            {
                order.order_state_id = ConfirmedOrderStatusId;
                stock.cantity--;
                this.orderService.UpdateOrder(order);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateErrorResponse(HttpStatusCode.NotModified, "Not modified");
        }

        private void SetupMappers()
        {
            var configModelMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderModel>()
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderState.state))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Product.price));
            });

            modelMapper = configModelMapper.CreateMapper();
        }
    }
}
