﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using ElectroShop.CustomAttributes;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;
using RoleEnum = ElectroShop.Enums;

namespace ElectroShop.Controllers
{
    public class StocksController : ApiController
    {
        private readonly IStockService stockService;

        private IMapper modelMapper;

        public StocksController(IStockService stockService)
        {
            this.stockService = stockService;
            SetupMappers();
        }

        [Route("api/stocks")]
        [UserAuthorize(RoleEnum.Role.Provider)]
        public IEnumerable<StockModel> GetAll()
        {
            var stocks = stockService.GetAllStock();
            var stockModels = modelMapper.Map<IEnumerable<Stock>, IEnumerable<StockModel>>(stocks);

            return stockModels;
        }

        [Route("api/stocks/{id}")]
        [UserAuthorize(RoleEnum.Role.Provider)]
        public StockModel GetById(int id)
        {
            var stock = stockService.GetById(id);
            var stockModel = modelMapper.Map<Stock, StockModel>(stock);

            return stockModel;
        }

        [HttpPut]
        [UserAuthorize(RoleEnum.Role.Provider)]
        [Route("api/stocks")]
        public HttpResponseMessage EditStock(StockModel model)
        {
            var stock = stockService.GetById(model.Id);
            stock.cantity = model.Cantity;
            stockService.UpdateProductStock(stock);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private void SetupMappers()
        {
            var modelMapperConfig = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<Stock, StockModel>()
                    .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.name))
                    .ForMember(dest => dest.Brand, opt => opt.MapFrom(src => src.Product.brand))
                    .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Product.Category.name));
                cnf.CreateMap<Product, ProductModel>();
                cnf.CreateMap<Category, CategoryModel>();
            });

            this.modelMapper = modelMapperConfig.CreateMapper();
        }
    }
}
