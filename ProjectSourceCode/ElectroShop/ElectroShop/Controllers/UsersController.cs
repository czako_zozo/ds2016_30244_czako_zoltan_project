﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using ElectroShop.CustomAttributes;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;
using RoleEnum = ElectroShop.Enums;

namespace ElectroShop.Controllers
{
    public class UsersController : ApiController
    {
        private const string ClientRole = "client";

        private readonly IUserService userService;
        private readonly IRoleService roleService;

        private IMapper modelMapper;
        private IMapper entityMapper;

        public UsersController(IUserService userService, IRoleService roleService)
        {
            this.userService = userService;
            this.roleService = roleService;
            SetupMappers();
        }

        [Route("api/users")]
        [UserAuthorize(RoleEnum.Role.Admin)]
        public IEnumerable<UserModel> GetUsers()
        {
            var users = userService.GetAllUser();
            var userModels = modelMapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(users);

            return userModels;
        }

        [Route("api/users/{id}")]
 //       [UserAuthorize(RoleEnum.Role.Admin)]
        public UserModel GetById(int id)
        {
            var users = userService.GetUserById(id);
            var userModel = modelMapper.Map<User, UserModel>(users);

            return userModel;
        }

        [Route("api/get-by-username/{username}/{password}")]
        public UserModel GetByUsername(string username, string password)
        {
            var users = userService.GetByUsername(username);
            var userModel = modelMapper.Map<User, UserModel>(users);
            if (!password.Equals(userModel.Pass))
                throw new Exception("Wrong Credentials");

            var role = userModel.UserRoles.First();
            var mainRole = roleService.GetAllRoles().First(r => r.id == role.RoleId);
            userModel.MainRole = mainRole.name;

            var usernameBytes = Encoding.UTF8.GetBytes(userModel.Username);
            userModel.Username = Convert.ToBase64String(usernameBytes);

            var passwordBytes = Encoding.UTF8.GetBytes(userModel.Pass);
            userModel.Pass = Convert.ToBase64String(passwordBytes);

            return userModel;
        }

        [HttpPost]
        [Route("api/users")]
        public HttpResponseMessage InsertUser(UserModel model)
        {
            var user = entityMapper.Map<UserModel, User>(model);
            var roles = roleService.GetAllRoles();
            var clientRoleModel = roles.First(r => r.name.Equals(ClientRole));
            var clientRole = new UserRole()
            {
                role_id = clientRoleModel.id,
                user_id = user.id
            };

            user.UserRoles.Add(clientRole);
            userService.InsertUser(user);

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPut]
        [Route("api/users")]
        public HttpResponseMessage EditUser(UserModel model)
        {
            var userFromDatabase = userService.GetByUsername(model.Username);
            var user = entityMapper.Map<UserModel, User>(model);
            user.UserRoles = userFromDatabase.UserRoles;
            user.Orders = userFromDatabase.Orders;
            userService.UpdateUser(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
 //       [UserAuthorize(RoleEnum.Role.Admin)]
        [Route("api/users/{id}")]
        public HttpResponseMessage DeleteUser(int id)
        {
            var user = userService.GetUserById(id);
            userService.DeleteUser(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private void SetupMappers()
        {
            var modelMapperConfig = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<UserRole, UserRoleModel>();
                cnf.CreateMap<User, UserModel>();
            });

            var entityMapperConfig = new MapperConfiguration(cnf =>
            {
                cnf.CreateMap<UserRoleModel, UserRole>();
                cnf.CreateMap<UserModel, User>();
            });

            this.modelMapper = modelMapperConfig.CreateMapper();
            entityMapper = entityMapperConfig.CreateMapper();
        }
    }
}
