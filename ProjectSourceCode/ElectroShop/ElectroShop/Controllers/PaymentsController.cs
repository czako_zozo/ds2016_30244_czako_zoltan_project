﻿using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using ElectroShop.Models;
using Repository.DatabaseEntities;
using Services.Interfaces;

namespace ElectroShop.Controllers
{
    public class PaymentsController : ApiController
    {
        private readonly IPaymentMethodService paymentMethodService;

        private IMapper modelMapper;

        public PaymentsController(IPaymentMethodService paymentMethodService)
        {
            this.paymentMethodService = paymentMethodService;
            SetupMappers();
        }

        [Route("api/payments")]
        public IEnumerable<PaymentMethodModel> GetPaymentMethods()
        {
            var paymentMethods = paymentMethodService.getAllMethods();
            var paymentMethodModels =
                modelMapper.Map<IEnumerable<PaymentMethod>, IEnumerable<PaymentMethodModel>>(paymentMethods);

            return paymentMethodModels;
        } 

        private void SetupMappers()
        {
            var configModelMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PaymentMethod, PaymentMethodModel>();
            });

            modelMapper = configModelMapper.CreateMapper();
        }
    }
}
