﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ElectronRepository.Interfaces;
using ElectroShop.Enums;
using Repository.Interfaces;
using Repository.Repositories;
using Services.Interfaces;
using Services.Services;
using AllowAnonymousAttribute = ElectroShop.CustomAttributes.AllowAnonymousAttribute;
using UserAuthorizeAttribute = ElectroShop.CustomAttributes.UserAuthorizeAttribute;

namespace ElectroShop.FilterAttributes
{
    public class RequiredAuthorizationAttribute : AuthorizationFilterAttribute
    {
        private readonly IUserService userService;

        public RequiredAuthorizationAttribute()
        {
            IUserRepository userRepository = new UserRepository();
            IRatingRepository ratingRepository = new RatingRepository();
            this.userService = new UserService(userRepository, ratingRepository);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            bool skipAuthorization = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
            bool hasActionFilter =
                actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<UserAuthorizeAttribute>().Any();
            bool hasControllerFilter =
                actionContext.ActionDescriptor.GetCustomAttributes<UserAuthorizeAttribute>().Any();

            if (!skipAuthorization && (hasActionFilter || hasControllerFilter))
            {
                AuthorizeUser(actionContext);
            }
        }

        private void AuthorizeUser(HttpActionContext actionContext)
        {
            var authHeader = actionContext.Request.Headers.Authorization;

            if (authHeader != null && authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(authHeader.Parameter))
            {
                string username, password;
                GetCredentials(authHeader.Parameter, out username, out password);

                var existingUser = userService.GetByUsername(username);
                if (existingUser != null && existingUser.pass.Equals(password))
                {
                    var actionRole = actionContext.ActionDescriptor.GetCustomAttributes<UserAuthorizeAttribute>().FirstOrDefault();
                    var controllerRole = actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<UserAuthorizeAttribute>().FirstOrDefault();

                    bool hasRole;
                    if (controllerRole != null)
                        hasRole = existingUser.UserRoles.Any(r => r.Role.name.Equals(controllerRole.Role.GetRoleName()));
                    else if (actionRole != null)
                        hasRole = existingUser.UserRoles.Any(r => r.Role.name.Equals(actionRole.Role.GetRoleName()));
                    else
                        hasRole = true;

                    if(!hasRole)
                        HandleUnauthorized();
                }
                else
                {
                    HandleUnauthorized();
                }
            }
            else
            {
                HandleUnauthorized();
            }
        }

        private void GetCredentials(string header, out string username, out string password)
        {
            var encoding = Encoding.GetEncoding("iso-8859-1");

            var split = header.Split(':');
            username = encoding.GetString(Convert.FromBase64String(split[0]));
            password = encoding.GetString(Convert.FromBase64String(split[1]));
        }

        private void HandleUnauthorized()
        {
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
    }
}