using Microsoft.Practices.Unity;
using System.Web.Http;
using ElectronRepository.Interfaces;
using Repository.Interfaces;
using Repository.Repositories;
using Services.Interfaces;
using Services.Services;
using Unity.WebApi;

namespace ElectroShop
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<IOrderService, OrderService>();
            container.RegisterType<IPaymentMethodService, PaymentMethodService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<IRoleService, RoleService>();
            container.RegisterType<IStockService, StockService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IRatingService, RatingService>();

            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IStockRepository, StockRepository>();
            container.RegisterType<IPaymentMethodRepository, PaymentMethodRepository>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IRoleRepository, RoleRepository>();
            container.RegisterType<IRatingRepository, RatingRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}