﻿using Services.Interfaces;

namespace ElectroShop.ExceptionHandlers
{
    public class CustomExceptionHandler
    {
        private readonly IException exception;

        public CustomExceptionHandler(IException exception)
        {
            this.exception = exception;
        }

        public ICustomResponse Handle()
        {
            return exception.GetResponse();
        }
    }
}