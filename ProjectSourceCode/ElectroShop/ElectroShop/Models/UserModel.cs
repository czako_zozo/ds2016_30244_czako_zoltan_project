﻿using System.Collections.Generic;

namespace ElectroShop.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Mail { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string MainRole { get; set; }

        public virtual IEnumerable<UserRoleModel> UserRoles { get; set; }
    }
}