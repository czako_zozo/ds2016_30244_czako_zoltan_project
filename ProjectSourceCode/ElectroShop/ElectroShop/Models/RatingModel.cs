﻿namespace ElectroShop.Models
{
    public class RatingModel
    {
        public string Username { get; set; }
        public int ProductId { get; set; }
        public int Rating { get; set; }
    }
}