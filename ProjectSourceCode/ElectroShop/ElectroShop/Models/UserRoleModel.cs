﻿namespace ElectroShop.Models
{
    public class UserRoleModel
    {
        public int? RoleId { get; set; }
        public int? UserId { get; set; }
    }
}