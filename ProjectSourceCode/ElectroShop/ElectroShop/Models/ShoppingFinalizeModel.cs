﻿namespace ElectroShop.Models
{
    public class ShoppingFinalizeModel
    {
        public int PaymentMethodId { get; set; }
        public int UserId { get; set; }
    }
}