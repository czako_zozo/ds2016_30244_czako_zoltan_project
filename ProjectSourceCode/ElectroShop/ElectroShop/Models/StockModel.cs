﻿namespace ElectroShop.Models
{
    public class StockModel
    {
        public int Id { get; set; }
        public int? Cantity { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string CategoryName { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}