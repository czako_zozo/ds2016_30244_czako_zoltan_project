﻿namespace ElectroShop.Models
{
    public class PaymentMethodModel
    {
        public int Id { get; set; }
        public string Method { get; set; }
    }
}