﻿namespace ElectroShop.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public string Brand { get; set; }
        public int? Rating { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public string Image { get; set; }
        public string FileName { get; set; }
        public string CategoryName { get; set; }
        public int Quantity { get; set; }

        public virtual CategoryModel Category { get; set; }
    }
}