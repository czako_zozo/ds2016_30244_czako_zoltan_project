﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Repository.DatabaseEntities;

namespace ElectroShop.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public System.DateTime? Data { get; set; }
        public string OrderStatus { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
    }
}