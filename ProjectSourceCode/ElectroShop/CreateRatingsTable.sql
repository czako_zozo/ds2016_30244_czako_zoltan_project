use ElectroShop;

create table Ratings(
Id int IDENTITY(1,1) PRIMARY KEY,
UserId int,
ProductId int,
Rating int,
FOREIGN KEY (UserId) references Users(id),
FOREIGN KEY (ProductId) references Products(id)
);