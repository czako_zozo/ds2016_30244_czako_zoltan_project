
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/04/2016 13:15:31
-- Generated from EDMX file: D:\University\AnIII\Sem II\PS\Proiect\Electron\ElectronRepository\Electron.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ElectroShop];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Orders__order_st__2B3F6F97]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK__Orders__order_st__2B3F6F97];
GO
IF OBJECT_ID(N'[dbo].[FK__Orders__payment___2A4B4B5E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK__Orders__payment___2A4B4B5E];
GO
IF OBJECT_ID(N'[dbo].[FK__Orders__product___29572725]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK__Orders__product___29572725];
GO
IF OBJECT_ID(N'[dbo].[FK__Orders__user_id__286302EC]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK__Orders__user_id__286302EC];
GO
IF OBJECT_ID(N'[dbo].[FK__Products__catego__1DE57479]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK__Products__catego__1DE57479];
GO
IF OBJECT_ID(N'[dbo].[FK__Stock__product_i__21B6055D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Stock] DROP CONSTRAINT [FK__Stock__product_i__21B6055D];
GO
IF OBJECT_ID(N'[dbo].[FK__UserRole__role_i__164452B1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK__UserRole__role_i__164452B1];
GO
IF OBJECT_ID(N'[dbo].[FK__UserRole__user_i__34C8D9D1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK__UserRole__user_i__34C8D9D1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Category]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Category];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[OrderState]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderState];
GO
IF OBJECT_ID(N'[dbo].[PaymentMethods]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaymentMethods];
GO
IF OBJECT_ID(N'[dbo].[Products]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Products];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Stock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stock];
GO
IF OBJECT_ID(N'[dbo].[UserRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserRole];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Categories'
CREATE TABLE [dbo].[Categories] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(50)  NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [id] int IDENTITY(1,1) NOT NULL,
    [user_id] int  NULL,
    [product_id] int  NULL,
    [payment_id] int  NULL,
    [order_state_id] int  NULL,
    [data] datetime  NULL,
    [bargain] float  NULL
);
GO

-- Creating table 'OrderStates'
CREATE TABLE [dbo].[OrderStates] (
    [id] int IDENTITY(1,1) NOT NULL,
    [state] varchar(50)  NULL
);
GO

-- Creating table 'PaymentMethods'
CREATE TABLE [dbo].[PaymentMethods] (
    [id] int IDENTITY(1,1) NOT NULL,
    [method] varchar(50)  NULL
);
GO

-- Creating table 'Products'
CREATE TABLE [dbo].[Products] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(100)  NULL,
    [price] float  NULL,
    [brand] varchar(50)  NULL,
    [rating] int  NULL,
    [data] datetime  NULL,
    [description] varchar(max)  NULL,
    [category_id] int  NULL,
    [image] varchar(200)  NULL,
    [fileName] varchar(200)  NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(20)  NOT NULL
);
GO

-- Creating table 'Stocks'
CREATE TABLE [dbo].[Stocks] (
    [id] int IDENTITY(1,1) NOT NULL,
    [cantity] int  NULL,
    [product_id] int  NULL
);
GO

-- Creating table 'UserRoles'
CREATE TABLE [dbo].[UserRoles] (
    [id] int IDENTITY(1,1) NOT NULL,
    [user_id] int  NULL,
    [role_id] int  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [id] int IDENTITY(1,1) NOT NULL,
    [mail] varchar(30)  NOT NULL,
    [firstname] varchar(20)  NOT NULL,
    [lastname] varchar(20)  NOT NULL,
    [username] varchar(20)  NOT NULL,
    [pass] varchar(50)  NOT NULL,
    [mobile] varchar(15)  NOT NULL,
    [country] varchar(30)  NULL,
    [city] varchar(30)  NULL,
    [street] varchar(30)  NULL,
    [number] varchar(10)  NULL,
    [IBAN] varchar(50)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [PK_Categories]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'OrderStates'
ALTER TABLE [dbo].[OrderStates]
ADD CONSTRAINT [PK_OrderStates]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'PaymentMethods'
ALTER TABLE [dbo].[PaymentMethods]
ADD CONSTRAINT [PK_PaymentMethods]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [PK_Products]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Stocks'
ALTER TABLE [dbo].[Stocks]
ADD CONSTRAINT [PK_Stocks]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'UserRoles'
ALTER TABLE [dbo].[UserRoles]
ADD CONSTRAINT [PK_UserRoles]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [category_id] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK__Products__catego__1DE57479]
    FOREIGN KEY ([category_id])
    REFERENCES [dbo].[Categories]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Products__catego__1DE57479'
CREATE INDEX [IX_FK__Products__catego__1DE57479]
ON [dbo].[Products]
    ([category_id]);
GO

-- Creating foreign key on [order_state_id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK__Orders__order_st__2B3F6F97]
    FOREIGN KEY ([order_state_id])
    REFERENCES [dbo].[OrderStates]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Orders__order_st__2B3F6F97'
CREATE INDEX [IX_FK__Orders__order_st__2B3F6F97]
ON [dbo].[Orders]
    ([order_state_id]);
GO

-- Creating foreign key on [payment_id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK__Orders__payment___2A4B4B5E]
    FOREIGN KEY ([payment_id])
    REFERENCES [dbo].[PaymentMethods]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Orders__payment___2A4B4B5E'
CREATE INDEX [IX_FK__Orders__payment___2A4B4B5E]
ON [dbo].[Orders]
    ([payment_id]);
GO

-- Creating foreign key on [product_id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK__Orders__product___29572725]
    FOREIGN KEY ([product_id])
    REFERENCES [dbo].[Products]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Orders__product___29572725'
CREATE INDEX [IX_FK__Orders__product___29572725]
ON [dbo].[Orders]
    ([product_id]);
GO

-- Creating foreign key on [user_id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK__Orders__user_id__286302EC]
    FOREIGN KEY ([user_id])
    REFERENCES [dbo].[Users]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Orders__user_id__286302EC'
CREATE INDEX [IX_FK__Orders__user_id__286302EC]
ON [dbo].[Orders]
    ([user_id]);
GO

-- Creating foreign key on [product_id] in table 'Stocks'
ALTER TABLE [dbo].[Stocks]
ADD CONSTRAINT [FK__Stock__product_i__21B6055D]
    FOREIGN KEY ([product_id])
    REFERENCES [dbo].[Products]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Stock__product_i__21B6055D'
CREATE INDEX [IX_FK__Stock__product_i__21B6055D]
ON [dbo].[Stocks]
    ([product_id]);
GO

-- Creating foreign key on [role_id] in table 'UserRoles'
ALTER TABLE [dbo].[UserRoles]
ADD CONSTRAINT [FK__UserRole__role_i__164452B1]
    FOREIGN KEY ([role_id])
    REFERENCES [dbo].[Roles]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__UserRole__role_i__164452B1'
CREATE INDEX [IX_FK__UserRole__role_i__164452B1]
ON [dbo].[UserRoles]
    ([role_id]);
GO

-- Creating foreign key on [user_id] in table 'UserRoles'
ALTER TABLE [dbo].[UserRoles]
ADD CONSTRAINT [FK__UserRole__user_i__34C8D9D1]
    FOREIGN KEY ([user_id])
    REFERENCES [dbo].[Users]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__UserRole__user_i__34C8D9D1'
CREATE INDEX [IX_FK__UserRole__user_i__34C8D9D1]
ON [dbo].[UserRoles]
    ([user_id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------